import { expressMiddleware } from '@apollo/server/express4';
import { server } from './server';
import express, { json, urlencoded } from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import { Context } from './graphql/context';

const app = express();

const main = async (): Promise<void> => {
  await server.start();

  app.use(
    cors<cors.CorsRequest>(),
    json(),
    urlencoded({ extended: true }),
    cookieParser(),
    expressMiddleware(server, {
      context: async ({ req, res }) => new Context(req, res),
    }),
  );

  app.listen(4000, () => {
    console.log('Server running on port 4000');
  });
};

void main();
