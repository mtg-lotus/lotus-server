import * as jwt from 'jsonwebtoken';
import moment from 'moment';

import { SERVER_SECRET } from '../config/env';

export interface JwtPayload extends jwt.JwtPayload {
  email: string
  scopes: string[]
  exp: number
}

export const sign = (email: string, scopes: string[], type: 'access' | 'refresh' = 'access'): string => {
  const payload: JwtPayload = {
    email,
    scopes,
    exp: type === 'access' ? moment().add(30, 'm').unix() : moment().add(30, 'd').unix(),
  };

  const token = jwt.sign(payload, SERVER_SECRET);

  return token;
};

export const verify = (token: string): JwtPayload | null => {
  let decoded: JwtPayload;

  try {
    decoded = jwt.verify(token, SERVER_SECRET) as JwtPayload;
  } catch (e) {
    return null;
  }

  return decoded;
};
