import { CardImageUri as PrismaCardImageUri } from 'nexus-prisma';

import { objectType } from 'nexus';

export const CardImageUri = objectType({
  name: PrismaCardImageUri.$name,
  description: PrismaCardImageUri.$description,
  definition (t) {
    t.field(PrismaCardImageUri.id);
    t.field(PrismaCardImageUri.small);
    t.field(PrismaCardImageUri.normal);
    t.field(PrismaCardImageUri.large);
    t.field(PrismaCardImageUri.png);
    t.field(PrismaCardImageUri.artCrop);
    t.field(PrismaCardImageUri.borderCrop);
    t.field(PrismaCardImageUri.cardFace);
  },
});
