import { Set as PrismaSet } from 'nexus-prisma';

import { objectType } from 'nexus';

export const Set = objectType({
  name: PrismaSet.$name,
  description: PrismaSet.$description,
  definition (t) {
    t.field(PrismaSet.id);
    t.field(PrismaSet.set);
    t.field(PrismaSet.setName);
    t.field(PrismaSet.setType);
    t.field(PrismaSet.setUri);
    t.field(PrismaSet.cards);
  },
});
