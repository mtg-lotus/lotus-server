import { Artist as PrismaArtist } from 'nexus-prisma';

import { objectType } from 'nexus';

export const Artist = objectType({
  name: PrismaArtist.$name,
  description: PrismaArtist.$description,
  definition (t) {
    t.field(PrismaArtist.id);
    t.field(PrismaArtist.name);
    t.field(PrismaArtist.cards);
  },
});
