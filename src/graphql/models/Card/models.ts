import { Card as PrismaCard } from 'nexus-prisma';

import { objectType } from 'nexus';

export const Card = objectType({
  name: PrismaCard.$name,
  description: PrismaCard.$description,
  definition (t) {
    t.field(PrismaCard.id);
    t.field(PrismaCard.name);
    t.field(PrismaCard.lang);
    t.field(PrismaCard.layout);
    t.field(PrismaCard.oracleId);
    t.field(PrismaCard.cmc);
    t.field(PrismaCard.colorIdentity);
    t.field(PrismaCard.keywords);
    t.field(PrismaCard.reserved);
    t.field(PrismaCard.finishes);
    t.field(PrismaCard.rarity);
    t.field(PrismaCard.cardFaces);
    t.field(PrismaCard.legalities);
    t.field(PrismaCard.set);
    t.field(PrismaCard.deckCard);
  },
});
