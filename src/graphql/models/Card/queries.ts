import { extendType, nonNull, stringArg } from 'nexus';
import type { Context } from '../../context';

export const CardQuery = extendType({
  type: 'Query',
  definition (t) {
    t.field('getCardById', {
      type: 'Card',
      args: {
        id: nonNull(stringArg()),
      },
      resolve: (_, { id }: { id: string }, ctx: Context) => {
        return ctx.prisma.card.findFirst({
          where: {
            id,
          },
        });
      },
    });
  },
});
