import { Price as PrismaPrice } from 'nexus-prisma';

import { objectType } from 'nexus';

export const Price = objectType({
  name: PrismaPrice.$name,
  description: PrismaPrice.$description,
  definition (t) {
    t.field(PrismaPrice.id);
    t.field(PrismaPrice.date);
    t.field(PrismaPrice.usd);
    t.field(PrismaPrice.usdFoil);
    t.field(PrismaPrice.usdEtched);
    t.field(PrismaPrice.eur);
    t.field(PrismaPrice.eurFoil);
    t.field(PrismaPrice.tix);
    t.field(PrismaPrice.card);
  },
});
