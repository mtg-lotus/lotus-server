import {
  Format as PrismaFormat,
  Color as PrismaColor,
  Legality as PrismaLegality,
  Finish as PrismaFinish,
  Rarity as PrismaRarity,
} from 'nexus-prisma';

import {
  enumType,
} from 'nexus';

export const Format = enumType(PrismaFormat);
export const Color = enumType(PrismaColor);
export const Legality = enumType(PrismaLegality);
export const Finish = enumType(PrismaFinish);
export const Rarity = enumType(PrismaRarity);
