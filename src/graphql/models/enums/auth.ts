import {
  enumType,
} from 'nexus';

import {
  Role as PrismaRole,
} from 'nexus-prisma';

export const Role = enumType(PrismaRole);
