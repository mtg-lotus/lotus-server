import {
  enumType,
} from 'nexus';

import {
  LossMethod as PrismaLossMethod,
} from 'nexus-prisma';

export const LossMethod = enumType(PrismaLossMethod);
