import { Legalities as PrismaLegalities } from 'nexus-prisma';

import { objectType } from 'nexus';

export const Legalities = objectType({
  name: PrismaLegalities.$name,
  description: PrismaLegalities.$description,
  definition (t) {
    t.field(PrismaLegalities.id);
    t.field(PrismaLegalities.standard);
    t.field(PrismaLegalities.future);
    t.field(PrismaLegalities.historic);
    t.field(PrismaLegalities.gladiator);
    t.field(PrismaLegalities.pioneer);
    t.field(PrismaLegalities.explorer);
    t.field(PrismaLegalities.modern);
    t.field(PrismaLegalities.legacy);
    t.field(PrismaLegalities.pauper);
    t.field(PrismaLegalities.vintage);
    t.field(PrismaLegalities.penny);
    t.field(PrismaLegalities.commander);
    t.field(PrismaLegalities.brawl);
    t.field(PrismaLegalities.historicbrawl);
    t.field(PrismaLegalities.alchemy);
    t.field(PrismaLegalities.paupercommander);
    t.field(PrismaLegalities.duel);
    t.field(PrismaLegalities.oldschool);
    t.field(PrismaLegalities.premodern);
    t.field(PrismaLegalities.card);
  },
});
