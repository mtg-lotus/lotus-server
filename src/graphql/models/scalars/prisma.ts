import {
  DateTime as PrismaDateTime,
} from 'nexus-prisma/scalars';

export const DateTime = PrismaDateTime;
