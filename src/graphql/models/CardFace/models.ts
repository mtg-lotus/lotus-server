import { CardFace as PrismaCardFace } from 'nexus-prisma';

import { objectType } from 'nexus';

export const CardFace = objectType({
  name: PrismaCardFace.$name,
  description: PrismaCardFace.$description,
  definition (t) {
    t.field(PrismaCardFace.id);
    t.field(PrismaCardFace.name);
    t.field(PrismaCardFace.manaCost);
    t.field(PrismaCardFace.typeLine);
    t.field(PrismaCardFace.oracleText);
    t.field(PrismaCardFace.colors);
    t.field(PrismaCardFace.flavorText);
    t.field(PrismaCardFace.cardImageUri);
    t.field(PrismaCardFace.artist);
    t.field(PrismaCardFace.card);
  },
});
