import { DeckCard as PrismaDeckCard } from 'nexus-prisma';

import { objectType } from 'nexus';

export const DeckCard = objectType({
  name: PrismaDeckCard.$name,
  description: PrismaDeckCard.$description,
  definition (t) {
    t.field(PrismaDeckCard.id);
    t.field(PrismaDeckCard.deck);
    t.field(PrismaDeckCard.card);
  },
});
