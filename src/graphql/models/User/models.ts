import { User as PrismaUser } from 'nexus-prisma';
import {
  objectType,
} from 'nexus';

export const User = objectType({
  name: PrismaUser.$name,
  description: PrismaUser.$description,
  definition (t) {
    t.field(PrismaUser.id);
    t.field(PrismaUser.username);
    t.field(PrismaUser.image);
    t.field(PrismaUser.email);
    t.field(PrismaUser.firstName);
    t.field(PrismaUser.lastName);
  },
});

export const UserLoginResult = objectType({
  name: 'UserLoginResult',
  definition (t) {
    t.nonNull.field({
      name: 'user',
      type: 'User',
    });
    t.nonNull.string('refreshToken');
  },
});
