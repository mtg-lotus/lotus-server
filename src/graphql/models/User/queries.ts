import {
  extendType,
  stringArg,
  nonNull,
} from 'nexus';
import type { Context } from '../../context';

export const UserQuery = extendType({
  type: 'Query',
  definition (t) {
    t.field('user', {
      type: 'User',
      args: {
        id: nonNull(stringArg()),
      },
      resolve: (_, { id }, ctx: Context) => {
        return ctx.prisma.user.findFirst({
          where: {
            id,
          },
        });
      },
    });
  },
});
