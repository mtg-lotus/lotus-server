import { type Role as PrismaRole } from '@prisma/client';
import bcrypt from 'bcrypt';

import {
  extendType,
  nonNull,
  stringArg,
  list,
} from 'nexus';
import { sign, verify } from '../../../utils/jwt';
import { type Context } from '../../context';
import { Role } from '../enums/auth';

interface CreateUserArgs {
  email: string
  username: string
  firstName: string
  lastName: string
  password: string
  scopes?: PrismaRole[]
}

interface LoginUserArgs {
  email: string
  password: string
}

export const UserMutation = extendType({
  type: 'Mutation',
  definition (t) {
    t.nonNull.field('createUser', {
      type: 'User',
      args: {
        email: nonNull(stringArg()),
        username: nonNull(stringArg()),
        firstName: nonNull(stringArg()),
        lastName: nonNull(stringArg()),
        password: nonNull(stringArg()),
        scopes: list(nonNull(Role)),
      },
      resolve: async (_root, { email, username, firstName, lastName, password, scopes }: CreateUserArgs, ctx: Context) => {
        const hashed = await bcrypt.hash(password, 10);

        return await ctx.prisma.user.create({
          data: {
            email,
            username,
            firstName,
            lastName,
            password: hashed,
            scopes: scopes ?? [],
          },
        });
      },
    });

    t.field('loginUser', {
      type: 'UserLoginResult',
      args: {
        email: nonNull(stringArg()),
        password: nonNull(stringArg()),
      },
      resolve: async (_root, { email, password }: LoginUserArgs, ctx: Context) => {
        const user = await ctx.prisma.user.findFirst({
          where: {
            email,
          },
        });

        if (!user || !await bcrypt.compare(password, user.password)) {
          return null;
        }

        const accessToken = sign(email, user.scopes, 'access');
        const refreshToken = sign(email, user.scopes, 'refresh');

        ctx.setCookie(`lotus-session-jwt=${accessToken}`);

        return {
          user,
          refreshToken,
        };
      },
    });

    t.field('refresh', {
      type: 'User',
      args: {
        refreshToken: nonNull(stringArg()),
      },
      resolve: async (_, { refreshToken }: { refreshToken: string }, ctx: Context) => {
        const decoded = verify(refreshToken);

        const user = await ctx.prisma.user.findFirst({
          where: {
            email: decoded?.email,
          },
        });

        if (!user) {
          throw new Error('Failed to find user');
        }

        const accessToken = sign(user.email, user.scopes, 'refresh');

        ctx.setCookie(`lotus-session-jwt=${accessToken}`);

        return user;
      },
    });
  },
});
