import type { Format } from '@prisma/client';
import {
  booleanArg,
  list,
  extendType, nonNull, stringArg,
} from 'nexus';
import type { Context } from '../../context';

interface CreateDeckArgs {
  name: string
  format: Format
  userId: string
  tags: string[]
  pub?: boolean
}

export const DeckMutation = extendType({
  type: 'Mutation',
  definition (t) {
    t.field('createDeck', {
      type: 'Deck',
      args: {
        name: nonNull(stringArg()),
        format: nonNull('Format'),
        userId: nonNull(stringArg()),
        pub: booleanArg(),
        tags: nonNull(list(stringArg())),
      },
      resolve: async (_, { name, format, userId, pub, tags }: CreateDeckArgs, ctx: Context) => {
        return await ctx.prisma.deck.create({
          data: {
            name,
            format,
            public: pub ?? true,
            tags,
            user: {
              connect: {
                id: userId,
              },
            },
          },
        });
      },
    });
  },
});
