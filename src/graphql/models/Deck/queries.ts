import {
  list,
  extendType,
  nonNull,
  stringArg,
  intArg,
} from 'nexus';
import type { Context } from '../../context';

interface SearchDecksByTagsArgs {
  tags: string[]
  skip?: number
  take?: number
}

interface DecksByUserIdArgs {
  userId: string
  skip?: number
  take?: number
}

export const DeckQuery = extendType({
  type: 'Query',
  definition (t) {
    t.field('searchDecksByTags', {
      type: 'DeckSearchResult',
      args: {
        tags: nonNull(list(stringArg())),
        skip: intArg(),
        take: intArg(),
      },
      resolve: async (_, { tags, skip, take }: SearchDecksByTagsArgs, ctx: Context) => {
        const total = await ctx.prisma.deck.count({
          where: {
            tags: {
              hasSome: tags,
            },
          },
        });
        return {
          decks: await ctx.prisma.deck.findMany({
            take: take ?? 100,
            skip: skip ?? 0,
            where: {
              tags: {
                hasSome: tags,
              },
            },
          }),
          totalResults: total,
        };
      },
    });

    t.field('decksByUserId', {
      type: 'DeckSearchResult',
      args: {
        userId: nonNull(stringArg()),
        skip: intArg(),
        take: intArg(),
      },
      resolve: async (_, { userId, skip, take }: DecksByUserIdArgs, ctx: Context) => {
        const total = await ctx.prisma.deck.count({
          where: {
            userId,
          },
        });
        return {
          decks: await ctx.prisma.deck.findMany({
            take: take ?? 100,
            skip: skip ?? 0,
            where: {
              userId,
            },
          }),
          totalResults: total,
        };
      },
    });
  },
});
