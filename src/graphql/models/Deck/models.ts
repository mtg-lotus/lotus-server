import { Deck as PrismaDeck } from 'nexus-prisma';
import { list, objectType, nonNull } from 'nexus';

export const Deck = objectType({
  name: PrismaDeck.$name,
  description: PrismaDeck.$description,
  definition (t) {
    t.field(PrismaDeck.id);
    t.field(PrismaDeck.name);
    t.field(PrismaDeck.format);
    t.field(PrismaDeck.cards);
    t.field(PrismaDeck.tags);
    t.field('user', {
      type: 'User',
    });
  },
});

export const DeckSearchResult = objectType({
  name: 'DeckSearchResult',
  definition (t) {
    t.field('decks', {
      type: nonNull(list('Deck')),
    });
    t.int('totalResults');
  },
});
