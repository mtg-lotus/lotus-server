import type { Request, Response } from 'express';

import { PrismaClient, type User } from '@prisma/client';
import { verify } from '../utils/jwt';

export class Context {
  public readonly prisma: PrismaClient;
  public user: Promise<User | null>;

  private readonly __request: Request;
  private readonly __response: Response;

  public get accessToken (): string | undefined {
    return this.__request.cookies['lotus-session-jwt'];
  }

  constructor (req: Request, res: Response) {
    this.prisma = new PrismaClient();
    this.__request = req;
    this.__response = res;

    this.user = this.__verifyUser();
  }

  public setCookie (cookie: string | string[]): void {
    this.__response.setHeader('Set-Cookie', cookie);
  }

  public async destroy (): Promise<void> {
    await this.prisma.$disconnect();
  }

  private async __verifyUser (): Promise<User | null> {
    if (!this.accessToken) {
      return null;
    }

    const decoded = verify(this.accessToken);

    if (!decoded) {
      return null;
    }

    const res = await this.prisma.user.findFirst({
      where: {
        email: decoded.email,
      },
    });

    return res;
  }
};
