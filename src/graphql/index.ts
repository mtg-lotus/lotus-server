import path from 'path';
import { makeSchema } from 'nexus';

import * as types from './models';

export const schema = makeSchema({
  types,
  contextType: {
    module: require.resolve(path.join(__dirname, 'context')),
    export: 'Context',
  },
  outputs: {
    typegen: path.join(__dirname, '..', '..', '@types', 'nexus-typegen', 'index.d.ts'),
    schema: path.join(__dirname, '..', '..', 'nexus', 'schema.graphql'),
  },
});
