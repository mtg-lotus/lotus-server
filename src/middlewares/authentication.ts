import type { Role } from '@prisma/client';
import { rule, shield } from 'graphql-shield';
import { type Context } from '../graphql/context';

const rules = {
  isAuthenticated: rule()(async (_, __, ctx: Context) => {
    const user = await ctx.user;

    if (!user) {
      return false;
    }

    return true;
  }),
  hasRole: (roles: Role[]) => rule()(async (_, __, ctx: Context) => {
    const user = await ctx.user;

    if (!user) {
      return false;
    }

    return roles.some((role) => user.scopes.includes(role));
  }),
  allow: rule()(() => true),
};

export const authMiddleware = shield({
  Query: {
    decksByUserId: rules.isAuthenticated,
    user: rules.hasRole(['ADMIN']),
  },
  Mutation: {
    createDeck: rules.isAuthenticated,
  },
}, {
  debug: true,
});
