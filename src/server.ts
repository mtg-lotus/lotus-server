import { ApolloServer } from '@apollo/server';
import { schema } from './graphql';
import { type Context } from './graphql/context';
import { applyMiddleware } from 'graphql-middleware';

import { authMiddleware } from './middlewares/authentication';

export const server = new ApolloServer<Context>({
  schema: applyMiddleware(schema, authMiddleware),
  // context: ({ req, res }) => new Context(req, res),
  plugins: [
    {
      async requestDidStart () {
        return {
          async willSendResponse ({ contextValue }) {
            await contextValue.destroy();
          },
        };
      },
    },
  ],
});
