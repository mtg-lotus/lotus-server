import { config as dotenvConfig } from 'dotenv';

dotenvConfig();

const assertExists = (name: string): string => {
  const val = process.env[name];

  if (!val) {
    throw new Error(`Env variable not set ${name}`);
  }

  return val;
};

export const SERVER_SECRET = assertExists('SERVER_SECRET');
