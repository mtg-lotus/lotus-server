/*
  Warnings:

  - You are about to drop the column `rulingsIri` on the `card` table. All the data in the column will be lost.
  - Added the required column `username` to the `user` table without a default value. This is not possible if the table is not empty.
  - Added the required column `rulingsUri` to the `card` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "auth"."user" ADD COLUMN     "username" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "cards"."card" DROP COLUMN "rulingsIri",
ADD COLUMN     "rulingsUri" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "groups"."playGroupInvitation" ALTER COLUMN "expires" SET DEFAULT NOW() + interval '30 minutes';
