/*
  Warnings:

  - You are about to drop the column `emailVerified` on the `user` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[username]` on the table `user` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterTable
ALTER TABLE "auth"."user" DROP COLUMN "emailVerified",
ALTER COLUMN "firstName" DROP NOT NULL,
ALTER COLUMN "lastName" DROP NOT NULL;

-- AlterTable
ALTER TABLE "groups"."playGroupInvitation" ALTER COLUMN "expires" SET DEFAULT NOW() + interval '30 minutes';

-- CreateIndex
CREATE UNIQUE INDEX "user_username_key" ON "auth"."user"("username");
