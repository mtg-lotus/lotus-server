-- CreateSchema
CREATE SCHEMA IF NOT EXISTS "auth";

-- CreateSchema
CREATE SCHEMA IF NOT EXISTS "cards";

-- CreateSchema
CREATE SCHEMA IF NOT EXISTS "groups";

-- CreateEnum
CREATE TYPE "auth"."role" AS ENUM ('ADMIN', 'USER');

-- CreateEnum
CREATE TYPE "cards"."color" AS ENUM ('WHITE', 'BLUE', 'BLACK', 'RED', 'GREEN');

-- CreateEnum
CREATE TYPE "cards"."legality" AS ENUM ('NOT_LEGAL', 'LEGAL', 'RESTRICTED', 'BANNED');

-- CreateEnum
CREATE TYPE "cards"."finish" AS ENUM ('FOIL', 'NONFOIL', 'ETCHED');

-- CreateEnum
CREATE TYPE "cards"."rarity" AS ENUM ('COMMON', 'UNCOMMON', 'RARE', 'MYTHIC', 'SPECIAL', 'BONUS');

-- CreateEnum
CREATE TYPE "cards"."format" AS ENUM ('STANDARD', 'FUTURE', 'HISTORIC', 'GLADIATOR', 'PIONEER', 'EXPLORER', 'MODERN', 'LEGACY', 'PAUPER', 'VINTAGE', 'PENNY', 'COMMANDER', 'BRAWL', 'HISTORIC_BRAWL', 'ALCHEMY', 'PAUPER_COMMANDER', 'DUEL', 'OLDSCHOOL', 'PREMODERN');

-- CreateEnum
CREATE TYPE "groups"."loss_method" AS ENUM ('MILL', 'COMBAT_DAMAGE', 'NON_COMBAT_DAMAGE', 'OTHER');

-- CreateTable
CREATE TABLE "auth"."user" (
    "id" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "emailVerified" TIMESTAMP(3),
    "image" TEXT,
    "firstName" TEXT NOT NULL,
    "lastName" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "scopes" "auth"."role"[],

    CONSTRAINT "user_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "auth"."account" (
    "id" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "provider" TEXT NOT NULL,
    "providerAccountId" TEXT NOT NULL,
    "refresh_token" TEXT,
    "access_token" TEXT,
    "expires_at" INTEGER,
    "token_type" TEXT,
    "scope" TEXT,
    "id_token" TEXT,
    "session_state" TEXT,

    CONSTRAINT "account_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "auth"."session" (
    "id" TEXT NOT NULL,
    "sessionToken" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "expires" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "session_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "auth"."verification_token" (
    "identifier" TEXT NOT NULL,
    "token" TEXT NOT NULL,
    "expires" TIMESTAMP(3) NOT NULL
);

-- CreateTable
CREATE TABLE "cards"."deck" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "format" "cards"."format" NOT NULL DEFAULT 'STANDARD',
    "user_id" TEXT NOT NULL,

    CONSTRAINT "deck_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "cards"."deck_card" (
    "id" SERIAL NOT NULL,
    "count" INTEGER NOT NULL,
    "deck_id" INTEGER NOT NULL,
    "card_id" TEXT NOT NULL,

    CONSTRAINT "deck_card_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "cards"."card" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "lang" TEXT NOT NULL,
    "uri" TEXT NOT NULL,
    "scryfall_uri" TEXT NOT NULL,
    "layout" TEXT NOT NULL,
    "oracle_id" TEXT NOT NULL,
    "cmc" DOUBLE PRECISION,
    "color_identity" "cards"."color"[],
    "keywords" TEXT[],
    "games" TEXT[],
    "reserved" BOOLEAN NOT NULL,
    "finishes" "cards"."finish"[],
    "oversized" BOOLEAN NOT NULL,
    "promo" BOOLEAN NOT NULL,
    "reprint" BOOLEAN NOT NULL,
    "variation" BOOLEAN NOT NULL,
    "rulings_uri" TEXT NOT NULL,
    "collector_number" TEXT NOT NULL,
    "digital" BOOLEAN NOT NULL,
    "rarity" "cards"."rarity" NOT NULL,
    "border_color" TEXT NOT NULL,
    "full_art" BOOLEAN NOT NULL,
    "textless" BOOLEAN NOT NULL,
    "booster" BOOLEAN NOT NULL,
    "story_spotlight" BOOLEAN NOT NULL,
    "edhrec_rank" INTEGER,
    "penny_rank" INTEGER,
    "set_id" TEXT NOT NULL,

    CONSTRAINT "card_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "cards"."card_face" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "flavor_name" TEXT,
    "mana_cost" TEXT,
    "type_line" TEXT,
    "printed_type_line" TEXT,
    "oracle_text" TEXT,
    "printed_text" TEXT,
    "colors" "cards"."color"[],
    "flavor_text" TEXT,
    "illustration_id" TEXT,
    "card_image_uri_id" INTEGER NOT NULL,
    "artist_id" TEXT,
    "card_id" TEXT NOT NULL,

    CONSTRAINT "card_face_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "cards"."card_image_uri" (
    "id" SERIAL NOT NULL,
    "small" TEXT NOT NULL,
    "normal" TEXT NOT NULL,
    "large" TEXT NOT NULL,
    "png" TEXT NOT NULL,
    "art_crop" TEXT NOT NULL,
    "border_crop" TEXT NOT NULL,

    CONSTRAINT "card_image_uri_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "cards"."legalities" (
    "id" SERIAL NOT NULL,
    "standard" "cards"."legality" NOT NULL,
    "future" "cards"."legality" NOT NULL,
    "historic" "cards"."legality" NOT NULL,
    "gladiator" "cards"."legality" NOT NULL,
    "pioneer" "cards"."legality" NOT NULL,
    "explorer" "cards"."legality" NOT NULL,
    "modern" "cards"."legality" NOT NULL,
    "legacy" "cards"."legality" NOT NULL,
    "pauper" "cards"."legality" NOT NULL,
    "vintage" "cards"."legality" NOT NULL,
    "penny" "cards"."legality" NOT NULL,
    "commander" "cards"."legality" NOT NULL,
    "brawl" "cards"."legality" NOT NULL,
    "historicbrawl" "cards"."legality" NOT NULL,
    "alchemy" "cards"."legality" NOT NULL,
    "paupercommander" "cards"."legality" NOT NULL,
    "duel" "cards"."legality" NOT NULL,
    "oldschool" "cards"."legality" NOT NULL,
    "premodern" "cards"."legality" NOT NULL,
    "card_id" TEXT NOT NULL,

    CONSTRAINT "legalities_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "cards"."artist" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,

    CONSTRAINT "artist_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "cards"."price" (
    "id" SERIAL NOT NULL,
    "date" TEXT NOT NULL,
    "usd" TEXT,
    "usd_foil" TEXT,
    "usd_etched" TEXT,
    "eur" TEXT,
    "eur_foil" TEXT,
    "tix" TEXT,
    "card_id" TEXT NOT NULL,

    CONSTRAINT "price_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "cards"."set" (
    "id" TEXT NOT NULL,
    "set" TEXT NOT NULL,
    "set_name" TEXT NOT NULL,
    "set_type" TEXT NOT NULL,
    "set_uri" TEXT NOT NULL,

    CONSTRAINT "set_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "groups"."play_group" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "userId" TEXT NOT NULL,

    CONSTRAINT "play_group_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "groups"."play_group_invitation" (
    "id" TEXT NOT NULL,
    "expires" TIMESTAMP(3) NOT NULL DEFAULT NOW() + interval '30 minutes',
    "play_group_id" TEXT NOT NULL,

    CONSTRAINT "play_group_invitation_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "groups"."play_group_user" (
    "id" SERIAL NOT NULL,
    "play_group_id" TEXT NOT NULL,
    "user_id" TEXT NOT NULL,

    CONSTRAINT "play_group_user_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "groups"."play_group_user_game" (
    "id" SERIAL NOT NULL,
    "ranking" INTEGER NOT NULL,
    "loss_method" "groups"."loss_method",
    "option_loss_description" TEXT,
    "game_id" INTEGER NOT NULL,
    "play_group_user_id" INTEGER NOT NULL,
    "deck_id" INTEGER,

    CONSTRAINT "play_group_user_game_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "groups"."game" (
    "id" SERIAL NOT NULL,
    "format" "cards"."format" NOT NULL,
    "other_players_left_on_winning_turn" INTEGER NOT NULL DEFAULT 1,
    "num_turns" INTEGER,
    "win_method" "groups"."loss_method" NOT NULL,
    "optional_win_description" TEXT,
    "winner_id" INTEGER,

    CONSTRAINT "game_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "user_email_key" ON "auth"."user"("email");

-- CreateIndex
CREATE UNIQUE INDEX "account_provider_providerAccountId_key" ON "auth"."account"("provider", "providerAccountId");

-- CreateIndex
CREATE UNIQUE INDEX "session_sessionToken_key" ON "auth"."session"("sessionToken");

-- CreateIndex
CREATE UNIQUE INDEX "verification_token_token_key" ON "auth"."verification_token"("token");

-- CreateIndex
CREATE UNIQUE INDEX "verification_token_identifier_token_key" ON "auth"."verification_token"("identifier", "token");

-- CreateIndex
CREATE UNIQUE INDEX "card_uri_key" ON "cards"."card"("uri");

-- CreateIndex
CREATE UNIQUE INDEX "card_scryfall_uri_key" ON "cards"."card"("scryfall_uri");

-- CreateIndex
CREATE INDEX "card_oracle_id_idx" ON "cards"."card"("oracle_id");

-- CreateIndex
CREATE INDEX "card_reserved_idx" ON "cards"."card"("reserved");

-- CreateIndex
CREATE UNIQUE INDEX "card_face_card_image_uri_id_key" ON "cards"."card_face"("card_image_uri_id");

-- CreateIndex
CREATE UNIQUE INDEX "legalities_card_id_key" ON "cards"."legalities"("card_id");

-- CreateIndex
CREATE INDEX "price_date_idx" ON "cards"."price"("date");

-- CreateIndex
CREATE UNIQUE INDEX "set_set_key" ON "cards"."set"("set");

-- CreateIndex
CREATE UNIQUE INDEX "set_set_name_key" ON "cards"."set"("set_name");

-- AddForeignKey
ALTER TABLE "auth"."account" ADD CONSTRAINT "account_userId_fkey" FOREIGN KEY ("userId") REFERENCES "auth"."user"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "auth"."session" ADD CONSTRAINT "session_userId_fkey" FOREIGN KEY ("userId") REFERENCES "auth"."user"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cards"."deck" ADD CONSTRAINT "deck_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "auth"."user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cards"."deck_card" ADD CONSTRAINT "deck_card_deck_id_fkey" FOREIGN KEY ("deck_id") REFERENCES "cards"."deck"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cards"."deck_card" ADD CONSTRAINT "deck_card_card_id_fkey" FOREIGN KEY ("card_id") REFERENCES "cards"."card"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cards"."card" ADD CONSTRAINT "card_set_id_fkey" FOREIGN KEY ("set_id") REFERENCES "cards"."set"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cards"."card_face" ADD CONSTRAINT "card_face_card_image_uri_id_fkey" FOREIGN KEY ("card_image_uri_id") REFERENCES "cards"."card_image_uri"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cards"."card_face" ADD CONSTRAINT "card_face_artist_id_fkey" FOREIGN KEY ("artist_id") REFERENCES "cards"."artist"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cards"."card_face" ADD CONSTRAINT "card_face_card_id_fkey" FOREIGN KEY ("card_id") REFERENCES "cards"."card"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cards"."legalities" ADD CONSTRAINT "legalities_card_id_fkey" FOREIGN KEY ("card_id") REFERENCES "cards"."card"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cards"."price" ADD CONSTRAINT "price_card_id_fkey" FOREIGN KEY ("card_id") REFERENCES "cards"."card"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups"."play_group" ADD CONSTRAINT "play_group_userId_fkey" FOREIGN KEY ("userId") REFERENCES "auth"."user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups"."play_group_invitation" ADD CONSTRAINT "play_group_invitation_play_group_id_fkey" FOREIGN KEY ("play_group_id") REFERENCES "groups"."play_group"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups"."play_group_user" ADD CONSTRAINT "play_group_user_play_group_id_fkey" FOREIGN KEY ("play_group_id") REFERENCES "groups"."play_group"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups"."play_group_user" ADD CONSTRAINT "play_group_user_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "auth"."user"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups"."play_group_user_game" ADD CONSTRAINT "play_group_user_game_game_id_fkey" FOREIGN KEY ("game_id") REFERENCES "groups"."game"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups"."play_group_user_game" ADD CONSTRAINT "play_group_user_game_play_group_user_id_fkey" FOREIGN KEY ("play_group_user_id") REFERENCES "groups"."play_group_user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups"."play_group_user_game" ADD CONSTRAINT "play_group_user_game_deck_id_fkey" FOREIGN KEY ("deck_id") REFERENCES "cards"."deck"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups"."game" ADD CONSTRAINT "game_winner_id_fkey" FOREIGN KEY ("winner_id") REFERENCES "groups"."play_group_user"("id") ON DELETE SET NULL ON UPDATE CASCADE;
