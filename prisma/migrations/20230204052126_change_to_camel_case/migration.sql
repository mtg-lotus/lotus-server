/*
  Warnings:

  - You are about to drop the column `border_color` on the `card` table. All the data in the column will be lost.
  - You are about to drop the column `collector_number` on the `card` table. All the data in the column will be lost.
  - You are about to drop the column `color_identity` on the `card` table. All the data in the column will be lost.
  - You are about to drop the column `edhrec_rank` on the `card` table. All the data in the column will be lost.
  - You are about to drop the column `full_art` on the `card` table. All the data in the column will be lost.
  - You are about to drop the column `oracle_id` on the `card` table. All the data in the column will be lost.
  - You are about to drop the column `penny_rank` on the `card` table. All the data in the column will be lost.
  - You are about to drop the column `rulings_uri` on the `card` table. All the data in the column will be lost.
  - You are about to drop the column `scryfall_uri` on the `card` table. All the data in the column will be lost.
  - You are about to drop the column `set_id` on the `card` table. All the data in the column will be lost.
  - You are about to drop the column `story_spotlight` on the `card` table. All the data in the column will be lost.
  - You are about to drop the column `user_id` on the `deck` table. All the data in the column will be lost.
  - You are about to drop the column `card_id` on the `legalities` table. All the data in the column will be lost.
  - You are about to drop the column `card_id` on the `price` table. All the data in the column will be lost.
  - You are about to drop the column `eur_foil` on the `price` table. All the data in the column will be lost.
  - You are about to drop the column `usd_etched` on the `price` table. All the data in the column will be lost.
  - You are about to drop the column `usd_foil` on the `price` table. All the data in the column will be lost.
  - You are about to drop the column `set_name` on the `set` table. All the data in the column will be lost.
  - You are about to drop the column `set_type` on the `set` table. All the data in the column will be lost.
  - You are about to drop the column `set_uri` on the `set` table. All the data in the column will be lost.
  - You are about to drop the column `num_turns` on the `game` table. All the data in the column will be lost.
  - You are about to drop the column `optional_win_description` on the `game` table. All the data in the column will be lost.
  - You are about to drop the column `other_players_left_on_winning_turn` on the `game` table. All the data in the column will be lost.
  - You are about to drop the column `win_method` on the `game` table. All the data in the column will be lost.
  - You are about to drop the column `winner_id` on the `game` table. All the data in the column will be lost.
  - You are about to drop the `account` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `session` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `verification_token` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `card_face` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `card_image_uri` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `deck_card` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `play_group` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `play_group_invitation` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `play_group_user` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `play_group_user_game` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[scryfallUri]` on the table `card` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[cardId]` on the table `legalities` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[setName]` on the table `set` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `borderColor` to the `card` table without a default value. This is not possible if the table is not empty.
  - Added the required column `collectorNumber` to the `card` table without a default value. This is not possible if the table is not empty.
  - Added the required column `fullArt` to the `card` table without a default value. This is not possible if the table is not empty.
  - Added the required column `oracleId` to the `card` table without a default value. This is not possible if the table is not empty.
  - Added the required column `rulingsIri` to the `card` table without a default value. This is not possible if the table is not empty.
  - Added the required column `scryfallUri` to the `card` table without a default value. This is not possible if the table is not empty.
  - Added the required column `setId` to the `card` table without a default value. This is not possible if the table is not empty.
  - Added the required column `storySpotlight` to the `card` table without a default value. This is not possible if the table is not empty.
  - Added the required column `public` to the `deck` table without a default value. This is not possible if the table is not empty.
  - Added the required column `userId` to the `deck` table without a default value. This is not possible if the table is not empty.
  - Added the required column `cardId` to the `legalities` table without a default value. This is not possible if the table is not empty.
  - Added the required column `cardId` to the `price` table without a default value. This is not possible if the table is not empty.
  - Added the required column `setName` to the `set` table without a default value. This is not possible if the table is not empty.
  - Added the required column `setType` to the `set` table without a default value. This is not possible if the table is not empty.
  - Added the required column `setUri` to the `set` table without a default value. This is not possible if the table is not empty.
  - Added the required column `winMethod` to the `game` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "groups"."lossMethod" AS ENUM ('MILL', 'COMBAT_DAMAGE', 'NON_COMBAT_DAMAGE', 'OTHER');

-- DropForeignKey
ALTER TABLE "auth"."account" DROP CONSTRAINT "account_userId_fkey";

-- DropForeignKey
ALTER TABLE "auth"."session" DROP CONSTRAINT "session_userId_fkey";

-- DropForeignKey
ALTER TABLE "cards"."card" DROP CONSTRAINT "card_set_id_fkey";

-- DropForeignKey
ALTER TABLE "cards"."card_face" DROP CONSTRAINT "card_face_artist_id_fkey";

-- DropForeignKey
ALTER TABLE "cards"."card_face" DROP CONSTRAINT "card_face_card_id_fkey";

-- DropForeignKey
ALTER TABLE "cards"."card_face" DROP CONSTRAINT "card_face_card_image_uri_id_fkey";

-- DropForeignKey
ALTER TABLE "cards"."deck" DROP CONSTRAINT "deck_user_id_fkey";

-- DropForeignKey
ALTER TABLE "cards"."deck_card" DROP CONSTRAINT "deck_card_card_id_fkey";

-- DropForeignKey
ALTER TABLE "cards"."deck_card" DROP CONSTRAINT "deck_card_deck_id_fkey";

-- DropForeignKey
ALTER TABLE "cards"."legalities" DROP CONSTRAINT "legalities_card_id_fkey";

-- DropForeignKey
ALTER TABLE "cards"."price" DROP CONSTRAINT "price_card_id_fkey";

-- DropForeignKey
ALTER TABLE "groups"."game" DROP CONSTRAINT "game_winner_id_fkey";

-- DropForeignKey
ALTER TABLE "groups"."play_group" DROP CONSTRAINT "play_group_userId_fkey";

-- DropForeignKey
ALTER TABLE "groups"."play_group_invitation" DROP CONSTRAINT "play_group_invitation_play_group_id_fkey";

-- DropForeignKey
ALTER TABLE "groups"."play_group_user" DROP CONSTRAINT "play_group_user_play_group_id_fkey";

-- DropForeignKey
ALTER TABLE "groups"."play_group_user" DROP CONSTRAINT "play_group_user_user_id_fkey";

-- DropForeignKey
ALTER TABLE "groups"."play_group_user_game" DROP CONSTRAINT "play_group_user_game_deck_id_fkey";

-- DropForeignKey
ALTER TABLE "groups"."play_group_user_game" DROP CONSTRAINT "play_group_user_game_game_id_fkey";

-- DropForeignKey
ALTER TABLE "groups"."play_group_user_game" DROP CONSTRAINT "play_group_user_game_play_group_user_id_fkey";

-- DropIndex
DROP INDEX "cards"."card_oracle_id_idx";

-- DropIndex
DROP INDEX "cards"."card_scryfall_uri_key";

-- DropIndex
DROP INDEX "cards"."legalities_card_id_key";

-- DropIndex
DROP INDEX "cards"."set_set_name_key";

-- AlterTable
ALTER TABLE "cards"."card" DROP COLUMN "border_color",
DROP COLUMN "collector_number",
DROP COLUMN "color_identity",
DROP COLUMN "edhrec_rank",
DROP COLUMN "full_art",
DROP COLUMN "oracle_id",
DROP COLUMN "penny_rank",
DROP COLUMN "rulings_uri",
DROP COLUMN "scryfall_uri",
DROP COLUMN "set_id",
DROP COLUMN "story_spotlight",
ADD COLUMN     "borderColor" TEXT NOT NULL,
ADD COLUMN     "collectorNumber" TEXT NOT NULL,
ADD COLUMN     "colorIdentity" "cards"."color"[],
ADD COLUMN     "edhrecRank" INTEGER,
ADD COLUMN     "fullArt" BOOLEAN NOT NULL,
ADD COLUMN     "oracleId" TEXT NOT NULL,
ADD COLUMN     "pennyRank" INTEGER,
ADD COLUMN     "rulingsIri" TEXT NOT NULL,
ADD COLUMN     "scryfallUri" TEXT NOT NULL,
ADD COLUMN     "setId" TEXT NOT NULL,
ADD COLUMN     "storySpotlight" BOOLEAN NOT NULL;

-- AlterTable
ALTER TABLE "cards"."deck" DROP COLUMN "user_id",
ADD COLUMN     "public" BOOLEAN NOT NULL,
ADD COLUMN     "tags" TEXT[],
ADD COLUMN     "userId" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "cards"."legalities" DROP COLUMN "card_id",
ADD COLUMN     "cardId" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "cards"."price" DROP COLUMN "card_id",
DROP COLUMN "eur_foil",
DROP COLUMN "usd_etched",
DROP COLUMN "usd_foil",
ADD COLUMN     "cardId" TEXT NOT NULL,
ADD COLUMN     "eurFoil" TEXT,
ADD COLUMN     "usdEtched" TEXT,
ADD COLUMN     "usdFoil" TEXT;

-- AlterTable
ALTER TABLE "cards"."set" DROP COLUMN "set_name",
DROP COLUMN "set_type",
DROP COLUMN "set_uri",
ADD COLUMN     "setName" TEXT NOT NULL,
ADD COLUMN     "setType" TEXT NOT NULL,
ADD COLUMN     "setUri" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "groups"."game" DROP COLUMN "num_turns",
DROP COLUMN "optional_win_description",
DROP COLUMN "other_players_left_on_winning_turn",
DROP COLUMN "win_method",
DROP COLUMN "winner_id",
ADD COLUMN     "numTurns" INTEGER,
ADD COLUMN     "optionalWinDescription" TEXT,
ADD COLUMN     "otherPlayersLeftOnWinningTurn" INTEGER NOT NULL DEFAULT 1,
ADD COLUMN     "winMethod" "groups"."lossMethod" NOT NULL,
ADD COLUMN     "winnerId" INTEGER;

-- DropTable
DROP TABLE "auth"."account";

-- DropTable
DROP TABLE "auth"."session";

-- DropTable
DROP TABLE "auth"."verification_token";

-- DropTable
DROP TABLE "cards"."card_face";

-- DropTable
DROP TABLE "cards"."card_image_uri";

-- DropTable
DROP TABLE "cards"."deck_card";

-- DropTable
DROP TABLE "groups"."play_group";

-- DropTable
DROP TABLE "groups"."play_group_invitation";

-- DropTable
DROP TABLE "groups"."play_group_user";

-- DropTable
DROP TABLE "groups"."play_group_user_game";

-- DropEnum
DROP TYPE "groups"."loss_method";

-- CreateTable
CREATE TABLE "cards"."deckCard" (
    "id" SERIAL NOT NULL,
    "count" INTEGER NOT NULL,
    "deckId" INTEGER NOT NULL,
    "cardId" TEXT NOT NULL,

    CONSTRAINT "deckCard_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "cards"."cardFace" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "flavorName" TEXT,
    "manaCost" TEXT,
    "typeLine" TEXT,
    "printedTypeLine" TEXT,
    "oracleText" TEXT,
    "printedText" TEXT,
    "colors" "cards"."color"[],
    "flavorText" TEXT,
    "illustrationId" TEXT,
    "cardImageUriId" INTEGER NOT NULL,
    "artistId" TEXT,
    "cardId" TEXT NOT NULL,

    CONSTRAINT "cardFace_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "cards"."cardImageUri" (
    "id" SERIAL NOT NULL,
    "small" TEXT NOT NULL,
    "normal" TEXT NOT NULL,
    "large" TEXT NOT NULL,
    "png" TEXT NOT NULL,
    "artCrop" TEXT NOT NULL,
    "borderCrop" TEXT NOT NULL,

    CONSTRAINT "cardImageUri_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "groups"."playGroup" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "userId" TEXT NOT NULL,

    CONSTRAINT "playGroup_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "groups"."playGroupInvitation" (
    "id" TEXT NOT NULL,
    "expires" TIMESTAMP(3) NOT NULL DEFAULT NOW() + interval '30 minutes',
    "playGroupId" TEXT NOT NULL,

    CONSTRAINT "playGroupInvitation_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "groups"."playGroupUser" (
    "id" SERIAL NOT NULL,
    "playGroupId" TEXT NOT NULL,
    "userId" TEXT NOT NULL,

    CONSTRAINT "playGroupUser_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "groups"."playGroupUserGame" (
    "id" SERIAL NOT NULL,
    "ranking" INTEGER NOT NULL,
    "lossMethod" "groups"."lossMethod",
    "optionLossDescription" TEXT,
    "gameId" INTEGER NOT NULL,
    "playGroupUserId" INTEGER NOT NULL,
    "deckId" INTEGER,

    CONSTRAINT "playGroupUserGame_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "cardFace_cardImageUriId_key" ON "cards"."cardFace"("cardImageUriId");

-- CreateIndex
CREATE UNIQUE INDEX "card_scryfallUri_key" ON "cards"."card"("scryfallUri");

-- CreateIndex
CREATE INDEX "card_oracleId_idx" ON "cards"."card"("oracleId");

-- CreateIndex
CREATE UNIQUE INDEX "legalities_cardId_key" ON "cards"."legalities"("cardId");

-- CreateIndex
CREATE UNIQUE INDEX "set_setName_key" ON "cards"."set"("setName");

-- AddForeignKey
ALTER TABLE "cards"."deck" ADD CONSTRAINT "deck_userId_fkey" FOREIGN KEY ("userId") REFERENCES "auth"."user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cards"."deckCard" ADD CONSTRAINT "deckCard_deckId_fkey" FOREIGN KEY ("deckId") REFERENCES "cards"."deck"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cards"."deckCard" ADD CONSTRAINT "deckCard_cardId_fkey" FOREIGN KEY ("cardId") REFERENCES "cards"."card"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cards"."card" ADD CONSTRAINT "card_setId_fkey" FOREIGN KEY ("setId") REFERENCES "cards"."set"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cards"."cardFace" ADD CONSTRAINT "cardFace_cardImageUriId_fkey" FOREIGN KEY ("cardImageUriId") REFERENCES "cards"."cardImageUri"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cards"."cardFace" ADD CONSTRAINT "cardFace_artistId_fkey" FOREIGN KEY ("artistId") REFERENCES "cards"."artist"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cards"."cardFace" ADD CONSTRAINT "cardFace_cardId_fkey" FOREIGN KEY ("cardId") REFERENCES "cards"."card"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cards"."legalities" ADD CONSTRAINT "legalities_cardId_fkey" FOREIGN KEY ("cardId") REFERENCES "cards"."card"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cards"."price" ADD CONSTRAINT "price_cardId_fkey" FOREIGN KEY ("cardId") REFERENCES "cards"."card"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups"."playGroup" ADD CONSTRAINT "playGroup_userId_fkey" FOREIGN KEY ("userId") REFERENCES "auth"."user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups"."playGroupInvitation" ADD CONSTRAINT "playGroupInvitation_playGroupId_fkey" FOREIGN KEY ("playGroupId") REFERENCES "groups"."playGroup"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups"."playGroupUser" ADD CONSTRAINT "playGroupUser_playGroupId_fkey" FOREIGN KEY ("playGroupId") REFERENCES "groups"."playGroup"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups"."playGroupUser" ADD CONSTRAINT "playGroupUser_userId_fkey" FOREIGN KEY ("userId") REFERENCES "auth"."user"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups"."playGroupUserGame" ADD CONSTRAINT "playGroupUserGame_gameId_fkey" FOREIGN KEY ("gameId") REFERENCES "groups"."game"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups"."playGroupUserGame" ADD CONSTRAINT "playGroupUserGame_playGroupUserId_fkey" FOREIGN KEY ("playGroupUserId") REFERENCES "groups"."playGroupUser"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups"."playGroupUserGame" ADD CONSTRAINT "playGroupUserGame_deckId_fkey" FOREIGN KEY ("deckId") REFERENCES "cards"."deck"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groups"."game" ADD CONSTRAINT "game_winnerId_fkey" FOREIGN KEY ("winnerId") REFERENCES "groups"."playGroupUser"("id") ON DELETE SET NULL ON UPDATE CASCADE;
